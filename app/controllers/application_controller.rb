class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  def hello
      render html: `
      <!--nav role="navigation" class="navbar navbar-default bg-faded"-->
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="nav-brand navbar-text" href="/">Aylon Spigel - Frontend Web Application Developer</a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mynavbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="nav navbar-nav navbar-right" id="mynavbar">
        <ul class="nav navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="./" active>Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./resume/">Resume</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./coachingplus/">Coaching</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./contactus/">Contact Us</a>
          </li>
        </ul>
      </div>
    </div>
  <!--/nav-->
      `
  end
end
